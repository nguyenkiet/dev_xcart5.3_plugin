# <?php if (!defined('LC_DS')) { die(); } ?>

XC\ThemeTweaker:
  tables: [theme_tweaker_template]
  columns: { view_lists: { list_override: 'list_override VARCHAR(255) NOT NULL', weight_override: 'weight_override INT NOT NULL', override_mode: 'override_mode INT NOT NULL' } }
  dependencies: {  }
st_parcel_shipments_manifests, order_capost_parcel_shipment_links, order_capost_parcel_shipment_tracking, order_capost_parcel_shipment_tracking_events, order_capost_parcel_shipment_tracking_options, order_capost_parcel_shipment_tracking_files, order_capost_parcel_shipment_link_storage, order_capost_parcel_items, order_capost_parcels, order_capost_office, k, capost_return_links, capost_return_link_storage, capost_return_items]
  columns: {  }
  dependencies: {  }
