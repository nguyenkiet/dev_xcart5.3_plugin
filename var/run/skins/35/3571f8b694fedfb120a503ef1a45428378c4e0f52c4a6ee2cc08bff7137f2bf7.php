<?php

/* keys_notice/body.twig */
class __TwigTemplate_7ec6206694c681256754ebf0be53402c5e9fabfdaf1b728df091073da9062547 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"keys-notice-form\">

  ";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Button\\Link", "label" => "Re-check", "location" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getRecheckURL", array(), "method"), "style" => "recheck-license"))), "html", null, true);
        echo "

  <div class=\"title\">";
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("License warning")), "html", null, true);
        echo "</div>

  ";
        // line 11
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isCoreWarning", array(), "method")) {
            // line 12
            echo "
  <div class=\"indent\">";
            // line 13
            echo call_user_func_array($this->env->getFunction('t')->getCallable(), array("The system has detected that the license key that was activated for your store is invalid."));
            echo "</div>

  <ul class=\"modules-list license-key\">
    <li class=\"module-info\">
      <div class=\"module-name\">";
            // line 17
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "coreLicense", array()), "title", array()), "html", null, true);
            echo "</div>
      <div class=\"module-reason\">";
            // line 18
            echo $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "coreLicense", array()), "message", array());
            echo "</div>
      <div class=\"module-action\">";
            // line 19
            if ($this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "coreLicense", array()), "url", array())) {
                echo "<a href=\"";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "coreLicense", array()), "url", array()), "html", null, true);
                echo "\" href=\"_blank\">";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Purchase")), "html", null, true);
                echo "</a>";
            }
            echo "</div>
    </li>
  </ul>

  ";
            // line 23
            if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getUnallowedModules", array(), "method")) {
                // line 24
                echo "    <div class=\"indent\">";
                echo call_user_func_array($this->env->getFunction('t')->getCallable(), array("Also the system has detected problems with the following modules"));
                echo "</div>
  ";
            }
            // line 26
            echo "
  ";
        } else {
            // line 28
            echo "
  ";
            // line 29
            if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isFraudStatusConfirmed", array(), "method")) {
                // line 30
                echo "    <div class=\"alert alert-warning\">";
                echo call_user_func_array($this->env->getFunction('t')->getCallable(), array("Unallowed modules will be disabled automatically on the next visit any page in the administrator interface."));
                echo "</div>
  ";
            }
            // line 32
            echo "
  <div class=\"indent\">";
            // line 33
            echo call_user_func_array($this->env->getFunction('t')->getCallable(), array("The system has detected one or more problems with some of the modules at your store:"));
            echo "</div>
  <ul class=\"indent\">
    <li>";
            // line 35
            echo call_user_func_array($this->env->getFunction('t')->getCallable(), array("inactive license key(s);"));
            echo "</li>
    <li>";
            // line 36
            echo call_user_func_array($this->env->getFunction('t')->getCallable(), array("installed module(s) not allowed by the type of X-Cart license activated at your store."));
            echo "</li>
   </ul>

  <div class=\"indent\">";
            // line 39
            echo call_user_func_array($this->env->getFunction('t')->getCallable(), array("Using the modules with this type of license violation is illegal"));
            echo "</div>

 ";
        }
        // line 42
        echo "
  <ul class=\"modules-list\">
    ";
        // line 44
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getUnallowedModules", array(), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
            // line 45
            echo "      <li class=\"module-info\">
        <div class=\"module-name\">";
            // line 46
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["module"], "title", array()), "html", null, true);
            echo "</div>
        <div class=\"module-reason\">";
            // line 47
            echo $this->getAttribute($context["module"], "message", array());
            echo "</div>
        <div class=\"module-action\">";
            // line 48
            if ($this->getAttribute($context["module"], "url", array())) {
                echo "<a href=\"";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["module"], "url", array()), "html", null, true);
                echo "\" target=\"_blank\">";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Purchase")), "html", null, true);
                echo "</a>";
            }
            echo "</div>
      </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "  </ul>

  <div class=\"indent buttons\">

    ";
        // line 55
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isDisplayPurchaseAllButton", array(), "method")) {
            // line 56
            echo "    ";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Button\\Link", "label" => "Purchase license", "location" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getPurchaseAllURL", array(), "method"), "blank" => "true", "style" => "purchase regular-main-button"))), "html", null, true);
            echo "
    ";
        }
        // line 58
        echo "
    ";
        // line 59
        if ( !$this->getAttribute((isset($context["this"]) ? $context["this"] : null), "hasLicense", array(), "method")) {
            // line 60
            echo "      ";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Button\\ActivateKey"))), "html", null, true);
            echo "
    ";
        }
        // line 62
        echo "    ";
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "hasLicense", array(), "method")) {
            // line 63
            echo "      ";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Button\\Addon\\EnterLicenseKey"))), "html", null, true);
            echo "
    ";
        }
        // line 65
        echo "
    <div class=\"text or\">";
        // line 66
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("or")), "html", null, true);
        echo "</div>

    ";
        // line 68
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isCoreWarning", array(), "method")) {
            // line 69
            echo "      <a href=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getBackToTrialURL", array(), "method"), "html", null, true);
            echo "\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Back to Trial mode")), "html", null, true);
            echo "</a>
    ";
        }
        // line 71
        echo "    ";
        if ( !$this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isCoreWarning", array(), "method")) {
            // line 72
            echo "      <a href=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getRemoveModulesURL", array(), "method"), "html", null, true);
            echo "\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Remove unlicensed modules")), "html", null, true);
            echo "</a>
    ";
        }
        // line 74
        echo "
  </div>

  <div class=\"indent contact-us text\">
    ";
        // line 78
        echo call_user_func_array($this->env->getFunction('t')->getCallable(), array("You can also contact our CR department for help with this issue", array("url" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getContactUsURL", array(), "method"))));
        echo "
  </div>

  <div class=\"clear\"></div>

</div>
";
    }

    public function getTemplateName()
    {
        return "keys_notice/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 78,  211 => 74,  203 => 72,  200 => 71,  192 => 69,  190 => 68,  185 => 66,  182 => 65,  176 => 63,  173 => 62,  167 => 60,  165 => 59,  162 => 58,  156 => 56,  154 => 55,  148 => 51,  133 => 48,  129 => 47,  125 => 46,  122 => 45,  118 => 44,  114 => 42,  108 => 39,  102 => 36,  98 => 35,  93 => 33,  90 => 32,  84 => 30,  82 => 29,  79 => 28,  75 => 26,  69 => 24,  67 => 23,  54 => 19,  50 => 18,  46 => 17,  39 => 13,  36 => 12,  34 => 11,  29 => 9,  24 => 7,  19 => 4,);
    }
}
/* {##*/
/*  # License keys notice popup dialog*/
/*  #}*/
/* */
/* <div class="keys-notice-form">*/
/* */
/*   {{ widget('\\XLite\\View\\Button\\Link', label='Re-check', location=this.getRecheckURL(), style='recheck-license') }}*/
/* */
/*   <div class="title">{{ t('License warning') }}</div>*/
/* */
/*   {% if this.isCoreWarning() %}*/
/* */
/*   <div class="indent">{{ t('The system has detected that the license key that was activated for your store is invalid.')|raw }}</div>*/
/* */
/*   <ul class="modules-list license-key">*/
/*     <li class="module-info">*/
/*       <div class="module-name">{{ this.coreLicense.title }}</div>*/
/*       <div class="module-reason">{{ this.coreLicense.message|raw }}</div>*/
/*       <div class="module-action">{% if this.coreLicense.url %}<a href="{{ this.coreLicense.url }}" href="_blank">{{ t('Purchase') }}</a>{% endif %}</div>*/
/*     </li>*/
/*   </ul>*/
/* */
/*   {% if this.getUnallowedModules() %}*/
/*     <div class="indent">{{ t('Also the system has detected problems with the following modules')|raw }}</div>*/
/*   {% endif %}*/
/* */
/*   {% else %}*/
/* */
/*   {% if this.isFraudStatusConfirmed() %}*/
/*     <div class="alert alert-warning">{{ t('Unallowed modules will be disabled automatically on the next visit any page in the administrator interface.')|raw }}</div>*/
/*   {% endif %}*/
/* */
/*   <div class="indent">{{ t('The system has detected one or more problems with some of the modules at your store:')|raw }}</div>*/
/*   <ul class="indent">*/
/*     <li>{{ t('inactive license key(s);')|raw }}</li>*/
/*     <li>{{ t('installed module(s) not allowed by the type of X-Cart license activated at your store.')|raw }}</li>*/
/*    </ul>*/
/* */
/*   <div class="indent">{{ t('Using the modules with this type of license violation is illegal')|raw }}</div>*/
/* */
/*  {% endif %}*/
/* */
/*   <ul class="modules-list">*/
/*     {% for module in this.getUnallowedModules() %}*/
/*       <li class="module-info">*/
/*         <div class="module-name">{{ module.title }}</div>*/
/*         <div class="module-reason">{{ module.message|raw }}</div>*/
/*         <div class="module-action">{% if module.url %}<a href="{{ module.url }}" target="_blank">{{ t('Purchase') }}</a>{% endif %}</div>*/
/*       </li>*/
/*     {% endfor %}*/
/*   </ul>*/
/* */
/*   <div class="indent buttons">*/
/* */
/*     {% if this.isDisplayPurchaseAllButton() %}*/
/*     {{ widget('\\XLite\\View\\Button\\Link', label='Purchase license', location=this.getPurchaseAllURL(), blank='true', style='purchase regular-main-button') }}*/
/*     {% endif %}*/
/* */
/*     {% if not this.hasLicense() %}*/
/*       {{ widget('XLite\\View\\Button\\ActivateKey') }}*/
/*     {% endif %}*/
/*     {% if this.hasLicense() %}*/
/*       {{ widget('\\XLite\\View\\Button\\Addon\\EnterLicenseKey') }}*/
/*     {% endif %}*/
/* */
/*     <div class="text or">{{ t('or') }}</div>*/
/* */
/*     {% if this.isCoreWarning() %}*/
/*       <a href="{{ this.getBackToTrialURL() }}">{{ t('Back to Trial mode') }}</a>*/
/*     {% endif %}*/
/*     {% if not this.isCoreWarning() %}*/
/*       <a href="{{ this.getRemoveModulesURL() }}">{{ t('Remove unlicensed modules') }}</a>*/
/*     {% endif %}*/
/* */
/*   </div>*/
/* */
/*   <div class="indent contact-us text">*/
/*     {{ t('You can also contact our CR department for help with this issue', {'url': this.getContactUsURL()})|raw }}*/
/*   </div>*/
/* */
/*   <div class="clear"></div>*/
/* */
/* </div>*/
/* */
