<?php

/* header/parts/nofollow.twig */
class __TwigTemplate_894737de3a58818e7de5412d3b5e99d5b1d4957788950a24d9fbaceaef7d1dea extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<meta name=\"ROBOTS\" content=\"NOFOLLOW\" />
";
    }

    public function getTemplateName()
    {
        return "header/parts/nofollow.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Header part*/
/*  #*/
/*  # @ListChild (list="head", weight="70")*/
/*  #}*/
/* */
/* <meta name="ROBOTS" content="NOFOLLOW" />*/
/* */
