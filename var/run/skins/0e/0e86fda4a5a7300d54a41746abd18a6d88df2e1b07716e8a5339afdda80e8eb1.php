<?php

/* language_selector_single/body.twig */
class __TwigTemplate_c8bd050bd6acf714b543fdc89e5b80a6d2300a4f56260048b57e53342ba7d220 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"single-language-selector\">
  <ul>
    <li>
      <label class=\"selected\" data-href=\"";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getChangeLanguageLink", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "currentLanguage", array())), "method"), "html", null, true);
        echo "\">";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "currentLanguage", array()), "name", array()), "html", null, true);
        echo "</label>
    </li>
    ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getActiveLanguages", array(), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 11
            echo "      <li>
        <label data-href=\"";
            // line 12
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getChangeLanguageLink", array(0 => $context["language"]), "method"), "html", null, true);
            echo "\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["language"], "name", array()), "html", null, true);
            echo "</label>
      </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "  </ul>
</div>
";
    }

    public function getTemplateName()
    {
        return "language_selector_single/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 15,  39 => 12,  36 => 11,  32 => 10,  25 => 8,  19 => 4,);
    }
}
/* {##*/
/*  # Language selector*/
/*  #}*/
/* */
/* <div class="single-language-selector">*/
/*   <ul>*/
/*     <li>*/
/*       <label class="selected" data-href="{{ this.getChangeLanguageLink(this.currentLanguage) }}">{{ this.currentLanguage.name }}</label>*/
/*     </li>*/
/*     {% for language in this.getActiveLanguages() %}*/
/*       <li>*/
/*         <label data-href="{{ this.getChangeLanguageLink(language) }}">{{ language.name }}</label>*/
/*       </li>*/
/*     {% endfor %}*/
/*   </ul>*/
/* </div>*/
/* */
