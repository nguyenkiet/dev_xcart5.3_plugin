<?php

/* header/parts/meta_generator.twig */
class __TwigTemplate_4892e133fbc5b30e577047bd38e6e3647d855467d62edabc5b42a421e341c0c9 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<meta name=\"Generator\" content=\"X-Cart 5\" />
";
    }

    public function getTemplateName()
    {
        return "header/parts/meta_generator.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Head list children*/
/*  #*/
/*  # @ListChild (list="head", weight="500")*/
/*  #}*/
/* */
/* <meta name="Generator" content="X-Cart 5" />*/
/* */
