<?php

/* common/tooltip.twig */
class __TwigTemplate_239766de8f7d6069410063cb13f700cc73ed12d37ef3571f5f288cf4eac60f11 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 5
        ob_start();
        // line 6
        echo "<span
        data-toggle=\"popover\"
        data-trigger=\"hover\"
        data-placement=\"";
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getParam", array(0 => "placement"), "method"), "html", null, true);
        echo "\"
        data-content=\"";
        // line 10
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getParam", array(0 => "helpWidget"), "method")) {
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getParam", array(0 => "helpWidget"), "method"), "html", null, true);
        } else {
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getParam", array(0 => "text"), "method"), "html", null, true);
        }
        echo "\"
        data-html=\"true\"
        data-help-id=\"";
        // line 12
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getHelpId", array(), "method"), "html", null, true);
        echo "\"
        data-delay=\"";
        // line 13
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getDelay", array(), "method"), "html", null, true);
        echo "\"
        v-xlite-tooltip
        class=\"tooltip-main\">

";
        // line 17
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isImageTag", array(), "method")) {
            // line 18
            echo "  <i ";
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getAttributesCode", array(), "method");
            echo "></i>
";
        } else {
            // line 20
            echo "  <span ";
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getAttributesCode", array(), "method");
            echo ">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getParam", array(0 => "caption"), "method"), "html", null, true);
            echo "</span>
";
        }
        // line 22
        echo "
</span>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    public function getTemplateName()
    {
        return "common/tooltip.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 22,  61 => 20,  55 => 18,  53 => 17,  46 => 13,  42 => 12,  33 => 10,  29 => 9,  24 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Tooltip widget*/
/*  #}*/
/* */
/* {% spaceless %}*/
/* <span*/
/*         data-toggle="popover"*/
/*         data-trigger="hover"*/
/*         data-placement="{{ this.getParam('placement') }}"*/
/*         data-content="{% if this.getParam('helpWidget') %}{{  this.getParam('helpWidget') }}{% else  %}{{ this.getParam('text') }}{% endif %}"*/
/*         data-html="true"*/
/*         data-help-id="{{ this.getHelpId() }}"*/
/*         data-delay="{{ this.getDelay() }}"*/
/*         v-xlite-tooltip*/
/*         class="tooltip-main">*/
/* */
/* {% if this.isImageTag() %}*/
/*   <i {{ this.getAttributesCode()|raw }}></i>*/
/* {% else %}*/
/*   <span {{ this.getAttributesCode()|raw }}>{{ this.getParam('caption') }}</span>*/
/* {% endif %}*/
/* */
/* </span>*/
/* {% endspaceless %}*/
/* */
