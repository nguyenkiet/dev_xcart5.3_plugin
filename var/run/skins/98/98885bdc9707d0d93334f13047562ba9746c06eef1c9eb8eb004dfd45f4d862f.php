<?php

/* button/switcher_standalone.twig */
class __TwigTemplate_1382cb08696b32cde49317924664fb03cc625c492fcc3e9eda30beed52862038 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"standalone-switch\">
    ";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "displayCommentedData", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getCommentedData", array(), "method")), "method"), "html", null, true);
        echo "
    ";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\FormField\\Input\\Checkbox\\OnOff", "label" => $this->getAttribute(        // line 8
(isset($context["this"]) ? $context["this"] : null), "getParam", array(0 => "label"), "method"), "labelHelp" => $this->getAttribute(        // line 9
(isset($context["this"]) ? $context["this"] : null), "getParam", array(0 => "labelHelp"), "method"), "value" => $this->getAttribute(        // line 10
(isset($context["this"]) ? $context["this"] : null), "getValue", array(), "method")))), "html", null, true);
        echo "
</div>

";
    }

    public function getTemplateName()
    {
        return "button/switcher_standalone.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 10,  29 => 9,  28 => 8,  27 => 7,  23 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Switcher button*/
/*  #}*/
/* */
/* <div class="standalone-switch">*/
/*     {{ this.displayCommentedData(this.getCommentedData()) }}*/
/*     {{ widget('XLite\\View\\FormField\\Input\\Checkbox\\OnOff', */
/*         label=this.getParam('label'),*/
/*         labelHelp=this.getParam('labelHelp'),*/
/*         value=this.getValue()) }}*/
/* </div>*/
/* */
/* */
