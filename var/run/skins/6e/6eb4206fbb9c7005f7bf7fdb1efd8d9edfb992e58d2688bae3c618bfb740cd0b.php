<?php

/* cache_management_actions/body.twig */
class __TwigTemplate_53fd6088883d0353b747e46b7152dba5fdb1adeca841fb419ecc337e6816113f extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<table class=\"cache-management-actions\">
    <tbody>
        ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getBodyLines", array(), "method"));
        foreach ($context['_seq'] as $context["idx"] => $context["line"]) {
            // line 8
            echo "            <tr>
                ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["line"], "columns", array()));
            foreach ($context['_seq'] as $context["idx"] => $context["column"]) {
                // line 10
                echo "                    <td class=\"column column-";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, twig_lower_filter($this->env, $this->getAttribute($context["column"], "serviceName", array())), "html", null, true);
                echo "\">
                        ";
                // line 11
                if ( !twig_test_empty($this->getAttribute($context["column"], "view", array()))) {
                    // line 12
                    echo "                            ";
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => $this->getAttribute($context["column"], "view", array()), "idx" => $context["idx"], "entity" => $this->getAttribute($context["line"], "entity", array()), "column" => $context["column"]))), "html", null, true);
                    echo "
                        ";
                } elseif ( !twig_test_empty($this->getAttribute(                // line 13
$context["column"], "template", array()))) {
                    // line 14
                    echo "                            ";
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array("template" => $this->getAttribute($context["column"], "template", array()), "idx" => $context["idx"], "entity" => $this->getAttribute($context["line"], "entity", array()), "column" => $context["column"]))), "html", null, true);
                    echo "
                        ";
                } else {
                    // line 16
                    echo "                            ";
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["column"], "value", array()), "html", null, true);
                    echo "
                        ";
                }
                // line 18
                echo "                    </td>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['idx'], $context['column'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 20
            echo "            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['idx'], $context['line'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "    </tbody>
</table>
";
    }

    public function getTemplateName()
    {
        return "cache_management_actions/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 22,  68 => 20,  61 => 18,  55 => 16,  49 => 14,  47 => 13,  42 => 12,  40 => 11,  35 => 10,  31 => 9,  28 => 8,  24 => 7,  19 => 4,);
    }
}
/* {##*/
/*  # Cache management action body template*/
/*  #}*/
/* */
/* <table class="cache-management-actions">*/
/*     <tbody>*/
/*         {% for idx, line in this.getBodyLines() %}*/
/*             <tr>*/
/*                 {% for idx, column in line.columns %}*/
/*                     <td class="column column-{{ column.serviceName|lower }}">*/
/*                         {% if column.view is not empty %}*/
/*                             {{ widget(column.view, idx=idx, entity=line.entity, column=column) }}*/
/*                         {% elseif column.template is not empty %}*/
/*                             {{ widget(template=column.template, idx=idx, entity=line.entity, column=column) }}*/
/*                         {% else %}*/
/*                             {{ column.value }}*/
/*                         {% endif %}*/
/*                     </td>*/
/*                 {% endfor %}*/
/*             </tr>*/
/*         {% endfor %}*/
/*     </tbody>*/
/* </table>*/
/* */
