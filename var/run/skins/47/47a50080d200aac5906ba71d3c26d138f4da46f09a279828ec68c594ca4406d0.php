<?php

/* cache_management_actions/cell/action.twig */
class __TwigTemplate_dcef5e1ebf2dcc609d11968a668384bdb44a61fd88037d85324d58f4083ff5bb extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "column", array()), "value", array()), 1 => $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "entity", array()), "viewParams", array())))), "html", null, true);
    }

    public function getTemplateName()
    {
        return "cache_management_actions/cell/action.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Action button*/
/*  #}*/
/* */
/* {{ widget(this.column.value, this.entity.viewParams) }}*/
