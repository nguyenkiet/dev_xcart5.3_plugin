<?php

/* layout/header/header.right.mobile.minicart.twig */
class __TwigTemplate_efcbf0a736a2acfb0de4a92cb63f66104e97658292fbb0df468c8d623307bcac extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<div class=\"lc-minicart-placeholder\"></div>
";
    }

    public function getTemplateName()
    {
        return "layout/header/header.right.mobile.minicart.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Header bar search box*/
/*  #*/
/*  # @ListChild (list="layout.header.right.mobile", weight="100")*/
/*  #}*/
/* */
/* <div class="lc-minicart-placeholder"></div>*/
/* */
