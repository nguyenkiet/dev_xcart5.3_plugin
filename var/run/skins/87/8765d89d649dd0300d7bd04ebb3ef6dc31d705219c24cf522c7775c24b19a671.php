<?php

/* layout/header/locale_menu/language.twig */
class __TwigTemplate_901939271559afe4bedf8021788c3c9522717ec7a2e3e375b01481eb50b590e7 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<ul class=\"locale_language-selector\">
    <li>
    \t";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\LanguageSelector\\Customer"))), "html", null, true);
        echo "
    </li>
</ul>";
    }

    public function getTemplateName()
    {
        return "layout/header/locale_menu/language.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 7,  19 => 4,);
    }
}
/* {##*/
/*  # language part*/
/*  #}*/
/* */
/* <ul class="locale_language-selector">*/
/*     <li>*/
/*     	{{ widget('XLite\\View\\LanguageSelector\\Customer') }}*/
/*     </li>*/
/* </ul>*/
