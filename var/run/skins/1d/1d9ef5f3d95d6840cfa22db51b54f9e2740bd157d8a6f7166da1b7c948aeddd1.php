<?php

/* header/parts/ie.twig */
class __TwigTemplate_7f9f5dcda851b51ee1a3ee352629abc44edfa5c4782f6fd8fbcacec73986f4f0 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\" />
";
    }

    public function getTemplateName()
    {
        return "header/parts/ie.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Header part*/
/*  #*/
/*  # @ListChild (list="head", weight="40")*/
/*  #}*/
/* */
/* <meta http-equiv="X-UA-Compatible" content="IE=Edge" />*/
/* */
