<?php

/* header/parts/meta_compat_ie.twig */
class __TwigTemplate_ce25db86acae915fc2b0a4dc36a637bf9bbe9d7595637e755cc73b5cf1aae08a extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\" />
";
    }

    public function getTemplateName()
    {
        return "header/parts/meta_compat_ie.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Head list children*/
/*  #*/
/*  # @ListChild (list="head", weight="800")*/
/*  #}*/
/* */
/* <meta http-equiv="X-UA-Compatible" content="IE=Edge" />*/
/* */
