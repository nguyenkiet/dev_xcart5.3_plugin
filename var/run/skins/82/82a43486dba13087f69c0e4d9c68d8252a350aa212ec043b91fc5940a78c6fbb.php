<?php

/* layout/header/header.bar.locale.twig */
class __TwigTemplate_3f7ac32f01880f6838ff8dd68322db8a00bb765a8b6a266cf7d2f3d931425f0c extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"header_bar-locale dropdown\">
\t<a data-target=\"#\" data-toggle=\"dropdown\">
\t\t";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "layout.header.bar.locale"))), "html", null, true);
        echo "
\t</a>
\t<div class=\"dropdown-menu\">
\t\t";
        // line 10
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "layout.header.bar.locale.menu"))), "html", null, true);
        echo "
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "layout/header/header.bar.locale.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 10,  24 => 7,  19 => 4,);
    }
}
/* {##*/
/*  # Header bar language menu*/
/*  #}*/
/* */
/* <div class="header_bar-locale dropdown">*/
/* 	<a data-target="#" data-toggle="dropdown">*/
/* 		{{ widget_list('layout.header.bar.locale') }}*/
/* 	</a>*/
/* 	<div class="dropdown-menu">*/
/* 		{{ widget_list('layout.header.bar.locale.menu') }}*/
/* 	</div>*/
/* </div>*/
