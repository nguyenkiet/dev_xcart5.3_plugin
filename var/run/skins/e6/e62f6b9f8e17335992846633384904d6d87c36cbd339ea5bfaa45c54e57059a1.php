<?php

/* layout/header/locale_menu/language_single.twig */
class __TwigTemplate_0797f9cd12c0d8c0fc95cc8adfc012486a7aaf916cf7ba22cc4ef13159517ca8 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<ul class=\"locale_language-selector\">
  <li>
    ";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\Module\\XC\\CrispWhiteSkin\\View\\LanguageSelector\\Customer"))), "html", null, true);
        echo "
  </li>
</ul>";
    }

    public function getTemplateName()
    {
        return "layout/header/locale_menu/language_single.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 7,  19 => 4,);
    }
}
/* {##*/
/*  # language part*/
/*  #}*/
/* */
/* <ul class="locale_language-selector">*/
/*   <li>*/
/*     {{ widget('XLite\\Module\\XC\\CrispWhiteSkin\\View\\LanguageSelector\\Customer') }}*/
/*   </li>*/
/* </ul>*/
