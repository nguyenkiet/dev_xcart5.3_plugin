<?php

/* cache_management_actions/cell/name.twig */
class __TwigTemplate_656b00cf2b0cbd01d54ca0c74e3fdfff52afab143d6c3a7bc42288feeb40ddf7 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"action-name\">
    ";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "entity", array()), "name", array()), "html", null, true);
        echo "
</div>

<div class=\"action-description\">
    ";
        // line 10
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "entity", array()), "description", array()), "html", null, true);
        echo "
</div>

";
        // line 13
        if ( !twig_test_empty($this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "entity", array()), "options", array()))) {
            // line 14
            echo "    <div class=\"action-options\">
        <table class=\"action-options-table\">
            ";
            // line 16
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "entity", array()), "options", array()));
            foreach ($context['_seq'] as $context["idx"] => $context["option"]) {
                // line 17
                echo "                <tr>
                    <td class=\"column-option\">
                        ";
                // line 19
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["option"], "name", array()), "html", null, true);
                echo "
                    </td>
                    <td class=\"column-option-action\">
                        ";
                // line 22
                if ( !twig_test_empty($this->getAttribute($context["option"], "view", array()))) {
                    // line 23
                    echo "                            ";
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => $this->getAttribute($context["option"], "view", array()), 1 => $this->getAttribute($context["option"], "viewParams", array())))), "html", null, true);
                    echo "
                        ";
                } elseif ( !twig_test_empty($this->getAttribute(                // line 24
(isset($context["column"]) ? $context["column"] : null), "template", array()))) {
                    // line 25
                    echo "                            ";
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array("template" => $this->getAttribute($context["option"], "template", array()), "params" => $this->getAttribute($context["option"], "viewParams", array())))), "html", null, true);
                    echo "
                        ";
                } else {
                    // line 27
                    echo "                            ";
                    echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["column"]) ? $context["column"] : null), "value", array()), "html", null, true);
                    echo "
                        ";
                }
                // line 29
                echo "                    </td>
                    <td class=\"column-option-help\">
                        <div class=\"help-wrapper\">
                            ";
                // line 32
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Tooltip", "text" => call_user_func_array($this->env->getFunction('t')->getCallable(), array($this->getAttribute($context["option"], "description", array()))), "className" => "help-icon"))), "html", null, true);
                echo "
                        </div>
                    </td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['idx'], $context['option'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 37
            echo "        </table>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "cache_management_actions/cell/name.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 37,  82 => 32,  77 => 29,  71 => 27,  65 => 25,  63 => 24,  58 => 23,  56 => 22,  50 => 19,  46 => 17,  42 => 16,  38 => 14,  36 => 13,  30 => 10,  23 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Name column template*/
/*  #}*/
/* */
/* <div class="action-name">*/
/*     {{ this.entity.name }}*/
/* </div>*/
/* */
/* <div class="action-description">*/
/*     {{ this.entity.description }}*/
/* </div>*/
/* */
/* {% if this.entity.options is not empty %}*/
/*     <div class="action-options">*/
/*         <table class="action-options-table">*/
/*             {% for idx, option in this.entity.options %}*/
/*                 <tr>*/
/*                     <td class="column-option">*/
/*                         {{ option.name }}*/
/*                     </td>*/
/*                     <td class="column-option-action">*/
/*                         {% if option.view is not empty %}*/
/*                             {{ widget(option.view, option.viewParams) }}*/
/*                         {% elseif column.template is not empty %}*/
/*                             {{ widget(template=option.template, params=option.viewParams) }}*/
/*                         {% else %}*/
/*                             {{ column.value }}*/
/*                         {% endif %}*/
/*                     </td>*/
/*                     <td class="column-option-help">*/
/*                         <div class="help-wrapper">*/
/*                             {{ widget('XLite\\View\\Tooltip', text=t(option.description), className='help-icon') }}*/
/*                         </div>*/
/*                     </td>*/
/*                 </tr>*/
/*             {% endfor %}*/
/*         </table>*/
/*     </div>*/
/* {% endif %}*/
