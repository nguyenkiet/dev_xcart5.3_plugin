<?php

/* header/parts/meta_mobile-capable.twig */
class __TwigTemplate_316f20484abbe5f990fc0c540a3ebdc9e3378db91b1b9552d62ca53e9c6ac658 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<meta name=\"apple-mobile-web-app-capable\"   content=\"yes\" />
<meta name=\"mobile-web-app-capable\"         content=\"yes\" />";
    }

    public function getTemplateName()
    {
        return "header/parts/meta_mobile-capable.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Head list children*/
/*  #*/
/*  # @ListChild (list="head", weight="700")*/
/*  #}*/
/* */
/* <meta name="apple-mobile-web-app-capable"   content="yes" />*/
/* <meta name="mobile-web-app-capable"         content="yes" />*/
