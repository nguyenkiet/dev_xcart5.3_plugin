<?php

/* header/parts/meta_viewport.twig */
class __TwigTemplate_95be6d4b1cad1bb2d7f1e518e1ae3afc782eb834e6a8fdf64d20ab21af44461c extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
";
    }

    public function getTemplateName()
    {
        return "header/parts/meta_viewport.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Head list children*/
/*  #*/
/*  # @ListChild (list="head", weight="650")*/
/*  #}*/
/* */
/* <meta name="viewport" content="width=device-width, initial-scale=1.0" />*/
/* */
