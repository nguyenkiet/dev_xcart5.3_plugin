<?php

/* header/parts/noindex.twig */
class __TwigTemplate_72d86b917c5015d18e12c2d00d09ff54529ef61a7b9b863271af2ce3f979bcff extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<meta name=\"robots\" content=\"noindex, nofollow\" />
";
    }

    public function getTemplateName()
    {
        return "header/parts/noindex.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Header part*/
/*  #*/
/*  # @ListChild (list="head", weight="40")*/
/*  #}*/
/* */
/* <meta name="robots" content="noindex, nofollow" />*/
/* */
