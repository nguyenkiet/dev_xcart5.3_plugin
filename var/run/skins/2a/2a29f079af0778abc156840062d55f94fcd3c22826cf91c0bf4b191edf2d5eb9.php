<?php

/* settings/base.twig */
class __TwigTemplate_994f3f2beea56f7bc79060711b62b3f4c28fe6341d88837585ec45e0f7128b3e extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\SettingsDialog"))), "html", null, true);
        echo "

";
    }

    public function getTemplateName()
    {
        return "settings/base.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 4,);
    }
}
/* {##*/
/*  # Common tab*/
/*  #}*/
/* {{ widget('XLite\\View\\SettingsDialog') }}*/
/* */
/* */
