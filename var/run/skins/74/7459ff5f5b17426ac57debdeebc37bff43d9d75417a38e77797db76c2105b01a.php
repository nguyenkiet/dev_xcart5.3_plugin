<?php

/* layout/header/mobile_header_parts/slidebar_menu.twig */
class __TwigTemplate_8a521a663cb434bce9a3583d14d372234ef6bf8c43f81f0d196fd08d5bc786e5 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<li class=\"dropdown mobile_header-slidebar\">
  <a id=\"main_menu\" href=\"#slidebar\" class=\"icon-menu\"></a>
</li>";
    }

    public function getTemplateName()
    {
        return "layout/header/mobile_header_parts/slidebar_menu.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 4,);
    }
}
/* {##*/
/*  # Header right box*/
/*  #}*/
/* */
/* <li class="dropdown mobile_header-slidebar">*/
/*   <a id="main_menu" href="#slidebar" class="icon-menu"></a>*/
/* </li>*/
