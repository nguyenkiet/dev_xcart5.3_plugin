<?php

/* header/parts/meta_mobile_capable.twig */
class __TwigTemplate_dfe39bf18aa37e3f9a67014a3e2b009fe3cf13dc750ffe3a628d36995810c4d1 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<meta name=\"apple-mobile-web-app-capable\"   content=\"yes\" />
<meta name=\"mobile-web-app-capable\"         content=\"yes\" />";
    }

    public function getTemplateName()
    {
        return "header/parts/meta_mobile_capable.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Head list children*/
/*  #*/
/*  # @ListChild (list="head", weight="700")*/
/*  #}*/
/* */
/* <meta name="apple-mobile-web-app-capable"   content="yes" />*/
/* <meta name="mobile-web-app-capable"         content="yes" />*/
