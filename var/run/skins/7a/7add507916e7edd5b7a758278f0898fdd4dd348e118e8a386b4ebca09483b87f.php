<?php

/* language_selector/body.twig */
class __TwigTemplate_a2976bd526d8a558f788573dd1edfad3b37ecc69f379da1dfcac1eb8085a02e4 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"group\">
    <label class=\"heading\">";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Language")), "html", null, true);
        echo "</label>
    <select class=\"form-control\" onChange=\"core.redirectTo(this.value)\">
      <option selected>";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "currentLanguage", array()), "name", array()), "html", null, true);
        echo "</option>
      ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getActiveLanguages", array(), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 10
            echo "        <option value=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getChangeLanguageLink", array(0 => $context["language"]), "method"), "html", null, true);
            echo "\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["language"], "name", array()), "html", null, true);
            echo "</option>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "    </select>
</div>
";
    }

    public function getTemplateName()
    {
        return "language_selector/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 12,  36 => 10,  32 => 9,  28 => 8,  23 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Language selector*/
/*  #}*/
/* */
/* <div class="group">*/
/*     <label class="heading">{{ t('Language') }}</label>*/
/*     <select class="form-control" onChange="core.redirectTo(this.value)">*/
/*       <option selected>{{ this.currentLanguage.name }}</option>*/
/*       {% for language in this.getActiveLanguages() %}*/
/*         <option value="{{ this.getChangeLanguageLink(language) }}">{{ language.name }}</option>*/
/*       {% endfor %}*/
/*     </select>*/
/* </div>*/
/* */
