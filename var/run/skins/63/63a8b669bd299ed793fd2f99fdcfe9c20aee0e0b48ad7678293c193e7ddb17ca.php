<?php

/* settings/cache_management.twig */
class __TwigTemplate_1cc031b2760d6375ccf7edb9b2ae46d451248f1c874c3ead423fc1828583805b extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 5
        if ( !$this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isQuickDataNotFinished", array(), "method")) {
            // line 6
            echo "  <div class=\"rebuilded-time\">
      <span>";
            // line 7
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Last time re-deployed at")), "html", null, true);
            echo " </span>
      <span class=\"time\" data-time=\"";
            // line 8
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLastRebuildTimeRaw", array(), "method"), "html", null, true);
            echo "\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLastRebuildTime", array(), "method"), "html", null, true);
            echo "</span>
  </div>
  <br>
  ";
            // line 11
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\ItemsList\\Model\\CacheManagementActions"))), "html", null, true);
            echo "
";
        }
        // line 13
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isQuickDataNotFinished", array(), "method")) {
            // line 14
            echo "  ";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\QuickData\\Progress"))), "html", null, true);
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "settings/cache_management.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 14,  44 => 13,  39 => 11,  31 => 8,  27 => 7,  24 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Cache management page*/
/*  #}*/
/* */
/* {% if not this.isQuickDataNotFinished() %}*/
/*   <div class="rebuilded-time">*/
/*       <span>{{ t('Last time re-deployed at') }} </span>*/
/*       <span class="time" data-time="{{ this.getLastRebuildTimeRaw() }}">{{ this.getLastRebuildTime() }}</span>*/
/*   </div>*/
/*   <br>*/
/*   {{ widget('\\XLite\\View\\ItemsList\\Model\\CacheManagementActions') }}*/
/* {% endif %}*/
/* {% if this.isQuickDataNotFinished() %}*/
/*   {{ widget('\\XLite\\View\\QuickData\\Progress') }}*/
/* {% endif %}*/
/* */
