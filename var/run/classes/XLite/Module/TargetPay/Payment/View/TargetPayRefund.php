<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TargetPay\Payment\View;

/**
 * Targetpay refunding function
 */
class TargetPayRefund extends \XLite\View\AView
{
    /**
     * Widget parameters
     */
    const PARAM_ORDER = 'order.operations';

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrderId()
    {
        return $this->getOrder()->getOrderId();
    }

    /**
     * Return default template
     *
     * @return string
     */
    protected function getDefaultTemplate()
    {
        return 'modules/TargetPay/Payment/RefundView.twig';
    }

    /**
     * Check widget visibility
     *
     * @return boolean
     */
    protected function isVisible()
    {
        // Check if order belong to Targetpay and success
        $targetpay_success = (new \XLite\Module\TargetPay\Payment\Model\TargetPaySale())->isPaymentSuccess($this->getOrder()->order_id);
        return !($targetpay_success && $this->isOrderSuccess());
    }

    protected function getTitle()
    {
        \XLite\Core\TopMessage::addError("The order has been rejected with the reason");

        $request = \XLite\Core\Request::getInstance();
        \XLite\Core\TopMessage::addError($request->amount);
    }
    /**
     * Get payment method of Order
     */
    protected function getPaymentMethod()
    {
        return \XLite\Core\Database::getRepo('XLite\Model\Payment\Method')->find(\XLite\Core\Request::getInstance()->id);
    }
    /**
     * Get current URL path
     */
    protected function getCurrentUrlPath()
    {
        return explode("?", \XLite\Core\URLManager::getCurrentURL())[0];
    }
    /**
     * Get current order number
     */
    protected function getOrderNumber()
    {
        $request = \XLite\Core\Request::getInstance();
        return $request->order_number;
    }
    /**
     * Check if Payment is success or not
     */
    protected function isOrderSuccess()
    {
        $transactions = $this->getOrder()->getPaymentTransactions();
        foreach ($transactions as $transaction) {
            if($transaction->isCompleted()){
                return true;
            }
        }
        return false;
    }
}
