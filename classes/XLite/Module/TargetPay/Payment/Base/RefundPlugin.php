<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\TargetPay\Payment\Base;

use XLite\Module\TargetPay\Payment\Base\TargetPayCore;
use XLite\Module\TargetPay\Payment\Model\TargetPaySale;
use XLite\Module\TargetPay\Payment\Model\TargetPayRefund;
/**
 * Targetpay refunding function
 */
class RefundPlugin extends \XLite\View\AView
{
    /**
     * Default Language for Targetpay
     * @var string
     */
    protected $language = "nl";
    /**
     * Targetpay AppID for refunding funtion
     * @var string
     */
    protected $appId = "382a92214fcbe76a32e22a30e1e9dd9f";
    /**
     * Widget parameters
     */
    const PARAM_ORDER = 'order.operations';

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrderId()
    {
        return $this->getOrder()->getOrderId();
    }

    /**
     * Return default template
     *
     * @return string
     */
    protected function getDefaultTemplate()
    {
        return 'modules/TargetPay/Payment/RefundView.twig';
    }

    /**
     * Check widget visibility
     *
     * @return boolean
     */
    protected function isVisible()
    {
        // Check if order belong to Targetpay and success
        $targetpay_success = (new \XLite\Module\TargetPay\Payment\Model\TargetPaySale())->getSuccessTransaction($this->getOrder()->order_id);
        return ($targetpay_success && $this->isOrderSuccess());
    }
    
    /**
     * Check if current Method support refund or not
     * @return boolean
     */
    protected function canRefund()
    {
        // Check Payment token setting
        /** @var \XLite\Model\Payment\Method $paymentMethod */
        $paymentMethod = $this->getPaymentMethod();
        if($paymentMethod) {
            return !empty($paymentMethod->getSetting("token")) && $this->getRemainRefundAmount() > 0;
        }
        return false;
    }
    /**
     * Do refund if any
     */
    protected function doRefundorDelete()
    {
        $request = \XLite\Core\Request::getInstance();
        $paymentMethod = $this->getPaymentMethod();
        $targetpay_sale = (new \XLite\Module\TargetPay\Payment\Model\TargetPaySale())->getSuccessTransaction($this->getOrderId());
        if ($paymentMethod->getSetting("mode") != 'live') {
            $testmode = true;
        } else {
            $testmode = false;
        }
        if(!empty($request->action_type))
        {
            /** @var \XLite\Model\Payment\Method $paymentMethod */
            $paymentMethod = $this->getPaymentMethod();
            if($request->action_type == "refund"){
                $refund_message = $request->message;
                $refund_amount = $request->amount;
                if(empty($refund_message)) 
                {
                    \XLite\Core\TopMessage::addError(static::t("Refunding message can't be empty."));                    
                } 
                else if(empty($refund_amount) || $refund_amount == 0) 
                {
                    \XLite\Core\TopMessage::addError(static::t("Refunding amount must be a number."));
                } 
                else 
                {
                    if($this->getRemainRefundAmount() <= 0 || (int) (100 * ((float)$this->getRemainRefundAmount())) < (int) (100 * ((float) $refund_amount)))
                    {
                        \XLite\Core\TopMessage::addError(static::t("The amount is invalid."));
                    }
                    else 
                    {
                        // Do Refund
                        $internalNote =  "Refunding Order with orderNumber: " . $this->getOrderNumber() . " - Targetpay transactionId: " . $targetpay_sale->targetpay_txid . " - Total price: " . $targetpay_sale->amount/100;
                        $consumerName = "GUEST";
                        if(!empty($this->getOrder()->getAddresses())) {
                            /** @var \XLite\Model\Address $add */
                            foreach ($this->getOrder()->getAddresses() as $add){
                                if($add->getIsBilling()) {
                                    $item = $this->getAddressSectionData($add);
                                    $consumerName = isset($item['firstname']) ? $item['firstname']['value'] : "";
                                    $consumerName .= isset($item['lastname']) ? " " . $item['lastname']['value'] : "";
                                }
                            }
                        }
                        // Build refund data
                        $refundData = array(
                            'paymethodID' => $targetpay_sale->method,
                            'transactionID' => $targetpay_sale->targetpay_txid,
                            'amount' => (int) ($refund_amount * 100), // Parse amount to Int and convert to cent value
                            'description' => $refund_message,
                            'internalNote' => $internalNote,
                            'consumerName' => $consumerName
                        );

                        $targetPay = new TargetPayCore($targetpay_sale->method, $paymentMethod->getSetting("rtlo"), $this->appId, $this->language, $testmode);
                        if(!$testmode && !$targetPay->refundInvoice($refundData, $paymentMethod->getSetting("token")))
                        {
                            \XLite\Core\TopMessage::addError("TargetPay refunding error: {$targetPay->getRawErrorMessage()}");
                        }
                        else
                        {
                            \XLite\Core\TopMessage::addInfo("Refunding has been placed successfully.");
                            // Insert to history log
                            $refund = new TargetPayRefund();
                            $refund->refund_id = $testmode ? time() : $targetPay->getRefundID();
                            $refund->order_id = $this->getOrderId();
                            $refund->transaction_id = $targetpay_sale->targetpay_txid;
                            $refund->refund_amount = $refund_amount;
                            $refund->refund_message = $refund_message;
                            $refund->status = "success";
                            $refund->datetimestamp = new \DateTime("now");
                            \XLite\Core\Database::getEM()->refresh($this->getOrder());
                            \XLite\Core\Database::getEM()->flush();
                            \XLite\Core\Database::getRepo('\XLite\Module\TargetPay\Payment\Model\TargetPayRefund')->insert($refund);
                            // Update payment status to Refunded
                            /** @var \XLite\Model\Payment\Transaction $transaction */
                            $transaction = $this->getTransaction();
                            // Add refund transaction
                            $transaction->createBackendTransaction(\XLite\Model\Payment\BackendTransaction::TRAN_TYPE_REFUND);
                            $transaction->getOrder()->setOldPaymentStatus($transaction->getOrder()->getPaymentStatus());
                            $paymentStatus = $this->getRemainRefundAmount() > 0 ? \XLite\Model\Order\Status\Payment::STATUS_PART_PAID : \XLite\Model\Order\Status\Payment::STATUS_REFUNDED;
                            $transaction->getOrder()->setPaymentStatus($paymentStatus);
                            // Update Order status
                            \XLite\Core\Database::getEM()->flush();
                        }
                    }                    
                }
            }
            else if($request->action_type == "cancel-refund") 
            {
                $refund_id = $request->refund_id;
                if(!empty($refund_id))
                {
                    // UPDATE ORDER STATUS TO PAID
                    \XLite\Core\Database::getEM()->refresh($this->getOrder());
                    \XLite\Core\Database::getEM()->flush();
                    
                    $refund_history = (new \XLite\Module\TargetPay\Payment\Model\TargetPayRefund())->findByRefundId($refund_id);
                    $targetPay = new TargetPayCore($targetpay_sale->method, $paymentMethod->getSetting("rtlo"), $this->appId, $this->language, $testmode);
                    if(!$testmode && !$targetPay->deleteRefund($refund_history->transaction_id, $paymentMethod->getSetting("token")))
                    {
                        \XLite\Core\TopMessage::addError("TargetPay cancelling refund error: {$targetPay->getRawErrorMessage()}");
                    } 
                    else 
                    {
                        // UPDATE REFUND TABLE FOR TRACKING
                        $refund_history->status = 'cancelled';
                        $refund_history->last_modified = new \DateTime("now");
                        \XLite\Core\Database::getRepo('\XLite\Module\TargetPay\Payment\Model\TargetPayRefund')->update($refund_history);
                        \XLite\Core\TopMessage::addInfo("Cancelling the refund has been updated successfully.");
                        // Check to update status of payment to paid or parially paid when cancelling refund
                        /** @var \XLite\Model\Payment\Transaction $transaction */
                        $transaction = $this->getTransaction();
                        // Add refund transaction
                        $transaction->setType(\XLite\Model\Payment\BackendTransaction::TRAN_TYPE_SALE);
                        $transaction->getOrder()->setOldPaymentStatus($transaction->getOrder()->getPaymentStatus());
                        $paymentStatus = $this->getRemainRefundAmount() == ($targetpay_sale->amount/100) ? \XLite\Model\Order\Status\Payment::STATUS_PAID : \XLite\Model\Order\Status\Payment::STATUS_PART_PAID;
                        $transaction->getOrder()->setPaymentStatus($paymentStatus);
                        // Update Order status
                        \XLite\Core\Database::getEM()->flush();                        
                    }
                }                
            }
            // Redirect to detail order page
            \XLite\Core\Operator::redirect($this->getOrderDetailUrl(), true);
        }
    }
    /**
     * 
     * @param unknown $orderStatus
     * @return boolean
     */
    protected function changeOrderStatus($orderStatus)
    {
        $cart = \XLite\Core\Database::getRepo('XLite\Model\Order')->find($this->getOrderId());
        if ($cart) {
            \XLite\Model\Cart::setObject($cart);
        } else {
            return false;
        }
        $cart->setPaymentStatus($orderStatus);
        \XLite\Core\Database::getEM()->flush();
        return true;
    }
    /**
     * Get refund history
     * 
     * @return \Doctrine\ORM\PersistentCollection|number
     */
    protected function getRefundHistory()
    {
        return (new \XLite\Module\TargetPay\Payment\Model\TargetPayRefund())->findByOrderId($this->getOrderId());
    }
    /**
     * Get refund available amount
     * @return number
     */
    protected function getRemainRefundAmount()
    {
        $targetpay_sale = (new \XLite\Module\TargetPay\Payment\Model\TargetPaySale())->getSuccessTransaction($this->getOrderId());
        $refund_histories = (new \XLite\Module\TargetPay\Payment\Model\TargetPayRefund())->findByOrderId($this->getOrderId());
        $total_refunded = 0;
        if(!empty($refund_histories)) {
            foreach ($refund_histories as $refund_item) {
                if($refund_item->status == "success"){
                    $total_refunded += $refund_item->refund_amount;
                }
            }
        }
        return $targetpay_sale->amount/100 - $total_refunded;
    }
    /**
     * Get order transaction
     * 
     * @return \XLite\Model\Payment\Transaction|NULL
     */
    protected function getTransaction()
    {        
        /** @var \XLite\Model\Payment\Method $paymentMethod */
        $paymentMethod = $this->getPaymentMethod();
        $transactions = $this->getOrder()->getPaymentTransactions();
        foreach ($transactions as $transaction) {
            /** @var \XLite\Model\Payment\Transaction $transaction */
            if($transaction->isCompleted() && $transaction->getPaymentMethod()->getMethodId() == $paymentMethod->getMethodId()){
                return $transaction;
            }
        }
        return null;
    }
    /**
     * Get payment method of Order
     */
    protected function getPaymentMethod()
    {
        $targetpay_sale = (new \XLite\Module\TargetPay\Payment\Model\TargetPaySale())->getSuccessTransaction($this->getOrderId());
        if($targetpay_sale) {
            return \XLite\Core\Database::getRepo('XLite\Model\Payment\Method')->find($targetpay_sale->method_id);
        }
        return null;
    }
    /**
     * Get current URL path
     */
    protected function getCurrentUrlPath()
    {
        return explode("?", \XLite\Core\URLManager::getCurrentURL())[0];
    }
    /**
     * Get current order number
     * @return string
     */
    protected function getOrderDetailUrl()
    {
        return $this->getCurrentUrlPath() . "?target=order&order_number=" . $this->getOrderNumber();        
    }
    /**
     * Get current order number
     */
    protected function getOrderNumber()
    {
        $request = \XLite\Core\Request::getInstance();
        return $request->order_number;
    }
    /**
     * Check if Payment is success or not
     */
    protected function isOrderSuccess()
    {
        return $this->getPaymentMethod() != null && $this->getTransaction() != null;
    }
    /**
     * Check to show refund history
     * @return boolean
     */
    protected function isShowRefundTitle()
    {
        return $this->canRefund() || !empty($this->getRefundHistory());
    }
    /**
     * Format datetime
     * {@inheritDoc}
     * @see \XLite\View\AView::formatDate()
     */
    protected function formatDateTime($val)
    {
        return $this->formatDate(strtotime($val)) . ", " . $this->formatDayTime(strtotime($val));//substr($val, 0, 16);
    }
}
